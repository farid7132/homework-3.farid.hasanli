## Answers:

##1.
 `var` is function-scoped.When we define a variable with `var` inside a function it can be changed inside that function.
If its is defined outside of a function it becomes global-scoped and can be changed from anywhere.

The variable declared with `let` is limited to the block defined with curly brackets - {}.

If we declare a variable with `const` it will also be scoped with the curly brace block.
But we won't be able to change or reassign this variable.

##2
As mentioned variables declared with `var` can be changed without our intention to do so.
For example if we declare same variable twice by mistake using `var` we will not get any error.
By declaring variables with `let` and `const` we are using them in their specific scope (block),
not function or global scope as with `var` and this makes our code more safer and readable.